from __future__ import unicode_literals

import sys
import logging


def _preppend_msg(rid, msg):
    return '<{}> {}'.format(rid, msg)

if sys.version_info > (3,1):
    def rid_filter(record):
        if hasattr(record, 'rid'):
            record.msg = _preppend_msg(record.rid, record.msg)
        return True

else:
    class RIDFilter(object):
        def filter(self, record):
            if hasattr(record, 'rid'):
                record.msg = _preppend_msg(record.rid, record.msg)
            return True

    rid_filter = RIDFilter()


def getLogger(name):
    l = logging.getLogger(name)
    l.addFilter(rid_filter)
    return l

# tmob_audit #

Models, Middlewares y m�s para los Djangos de la empresa.

### ChangeLog ###

#### 1.3.2 ####

* Se agrega unicode_literals al m�dulo de log

#### 1.3.1 ####

* Fix de cuarta en el setup.py: py_modules vs packages!

#### 1.3.0 ####

* getLogger con filtro para RequestID

#### 1.2.3 ####

* middleware.RequestDataLogger cambia formato de impresi�n de RequestID

#### 1.2.2 ####

* Se agregan middlerares: RequestID y RequestDataLogger

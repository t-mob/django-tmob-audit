# coding: utf-8

from datetime import datetime
from django.conf import settings
import logging
import shortuuid

logger = logging.getLogger(settings.LOGGER_NAME.format(__name__))
RDL_URL_PREFIXES = getattr(settings, 'REQUESTDATALOGGER_URL_PREFIXES', None)


class TimeStampMiddleware(object):

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        request.timestamp = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')

        response = self.get_response(request)
        return response


class TimeStampOldStyleMiddleware(object):

    def process_request(self, request):
        request.timestamp = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
        return request


class RequestIDMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        rid = request.META.get('HTTP_X_REQUEST_ID')

        if rid is None:
            rid = shortuuid.uuid()

        request.rid = rid
        return self.get_response(request)


class RequestDataLogger(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def _match_url_prefixes(self, url):
        if RDL_URL_PREFIXES is None:
            """Por defecto, si no hay setting, loggeamos todo"""
            return True

        for prefix in RDL_URL_PREFIXES:
            if url.startswith(prefix):
                return True

        return False

    def __call__(self, request):

        if self._match_url_prefixes(request.path):

            rid = getattr(request, 'rid', None)
            headers = {k: v for k, v in request.META.items()
                       if k.startswith('HTTP_')}
            body = request.body

            msg = '{} {}'.format(request.method, request.path)

            if rid:
                msg += ' <{}>'.format(rid)

            if headers:
                msg += ' [Headers {}]'.format(headers)

            if body:
                msg += ' [Body {}]'.format(body)

            logger.debug(msg)

        return self.get_response(request)

#!/usr/bin/env python
# coding: utf-8

from setuptools import setup

setup(
    name='django-tmob-audit',
    version='1.3.2',
    description='T-Mob Django Audit Model',
    author='Ignacio Juan Mart?n Benedetti',
    author_email='ignacio.benedetti@t-mobs.com',
    packages=['tmob_audit'],
    install_requires=[
        'Django>=1.3',
        'shortuuid',
    ],
)
